package sheridan;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.sun.tools.javac.util.List;

public class MealsServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public  void testGetAvailableMealTypes() {
		java.util.List<String> typeOfMeal =  MealsService.getAvailableMealTypes(MealType.DRINKS);
		assertTrue("Valid value", typeOfMeal != null);
		
	}
	
	@Test
	public void testGetAvailableMealTypesException() {
		java.util.List<String> typeOfMeal =  MealsService.getAvailableMealTypes(null);
		assertTrue("Invalid value", typeOfMeal.get(0).equals("No Brand Available"));
		
	}
	@Test
	public void testGetAvailableMealTypesBoundaryIn() {
		java.util.List<String> typeOfMeal =  MealsService.getAvailableMealTypes(MealType.DRINKS);
		assertTrue("Invalid value", typeOfMeal.size() > 2);
	}
	
	@Test
	public void testGetAvailableMealTypesBoundaryOut() {
		java.util.List<String> typeOfMeal =  MealsService.getAvailableMealTypes(null);
		assertTrue("Invalid value", typeOfMeal.size() == 1);
	}
	

}
